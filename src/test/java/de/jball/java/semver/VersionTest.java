package de.jball.java.semver;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class VersionTest {

	public static Stream<Arguments> testCompareTo() {
		Version same = Version.of(1, 2, 3);
		return Stream.of(
				arguments(Version.of(1, 2, 3), Version.of(1, 2, 3), 0),
				arguments(Version.of("1.2.3"), Version.of(1, 2, 3), 0),
				arguments(Version.of(1, 2, 3), Version.of(1, 2, 4), -1),
				arguments(Version.of("1.2.3"), Version.of(1, 2, 3, "alpha.3"), 1),
				arguments(Version.of("1.2.3-alpha"), Version.of(1, 2, 3, "beta"), -1),
				arguments(Version.of("1.0.0-beta.2"), Version.of("1.0.0-beta"), 1),
				arguments(same, same, 0),
				arguments(Version.of(1, 3, 6, "beta.2", "b89bfe"), Version.of("1.3.6-beta.2+b89bfe"), 0),
				arguments(Version.of(1, 3, 6, "beta.2", "b89bfe"), Version.of("1.3.6-beta.2+b89bfg"), 0)
		);
	}

	public static Stream<Arguments> testEquals() {
		Version same = Version.of(1, 3, 5, "alpha", "b4d7b");
		return Stream.of(
				arguments(Version.of(1, 2, 3), Version.of(1, 2, 3), true),
				arguments(Version.of(1, 2, 3), Version.of(1, 2, 4), false),
				arguments(Version.of(1, 2, 3), new Object(), false),
				arguments(Version.of(1, 2, 3), null, false),
				arguments(same, same, true),
				arguments(Version.of(1, 3, 6, "beta.2", "b89bfe"), Version.of("1.3.6-beta.2+b89bfe"), true),
				arguments(Version.of(1, 3, 6, "beta.2", "b89bfe"), Version.of("1.3.6-beta.2+b89bfg"), false)
		);
	}

	public static Stream<Arguments> testToString() {
		return Stream.of(
				arguments(Version.of(1, 2, 3), "1.2.3"),
				arguments(Version.of(1, 2, 3, "alpha.1"), "1.2.3-alpha.1"),
				arguments(Version.of(1, 2, 3, null, "sha45759714"), "1.2.3+sha45759714")
		);
	}

	public static Stream<Arguments> testFailingBuildMetadata() {
		return Stream.of(
				arguments("+asdf"),
				arguments(".1234")
		);
	}

	public static Stream<Arguments> testFailingPreRelease() {
		return Stream.of(
				arguments("..."),
				arguments("äää")
		);
	}

	@ParameterizedTest
	@MethodSource
	void testCompareTo(Version a, Version b, int expectedSignum) {
		assertEquals(expectedSignum, Integer.signum(a.compareTo(b)));
	}

	@Test
	void testCompareToNull() {
		Version version = Version.of(1, 2, 3);
		assertThrows(NullPointerException.class, () -> version.compareTo(null),
				"Comparing to null did not throw an Exception.");
	}

	@ParameterizedTest
	@MethodSource
	void testEquals(Version a, Object b, boolean expected) {
		assertEquals(expected, a.equals(b));
	}

	@ParameterizedTest
	@MethodSource
	void testToString(Version a, String expected) {
		assertEquals(expected, a.toString());
	}

	@Test
	void testHashCode() {
		assertTrue(Version.of(1, 2, 3).hashCode() > 0);
	}

	@ParameterizedTest
	@MethodSource
	void testFailingBuildMetadata(String buildMetadata) {
		assertThrows(IllegalArgumentException.class, () -> Version.of(1, 2, 3, null, buildMetadata));
	}

	@ParameterizedTest
	@MethodSource
	void testFailingPreRelease(String preRelease) {
		assertThrows(IllegalArgumentException.class, () -> Version.of(1, 2, 3, preRelease));
	}
}
