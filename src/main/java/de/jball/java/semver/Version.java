package de.jball.java.semver;

import lombok.NonNull;
import lombok.Value;

import javax.annotation.RegEx;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This represents a Version object. It is immutable and can be instantiated by the static factory Methods {@code of}
 */
@Value
public class Version implements Serializable, Comparable<Version> {

	private static final Comparator<Version> COMPARATOR = Comparator.comparing(Version::getMajor).thenComparing(Version::getMinor).thenComparing(Version::getPatch).thenComparing(Version::getPreRelease, Comparator.nullsLast(Comparator.naturalOrder()));

	@RegEx
	private static final String PRE_RELEASE_PATTERN_STRING = "(0|[1-9]\\d*|\\d*[A-Za-z-][\\dA-Za-z-]*)(\\.(0|[1-9]\\d*|\\d*[A-Za-z-][\\dA-Za-z-]*))*";
	private static final Pattern PRE_RELEASE_PATTERN = Pattern.compile("^(" + PRE_RELEASE_PATTERN_STRING + ")$");

	@RegEx
	private static final String BUILD_METADATA_PATTERN_STRING = "[\\dA-Za-z-]+(\\.[\\dA-Za-z-]*)*";
	private static final Pattern BUILD_METADATA_PATTERN = Pattern.compile("^(" + BUILD_METADATA_PATTERN_STRING + ")$");

	/**
	 * Regex pattern as defined on the <a href="https://semver.org/">Semver website.</a>
	 *
	 * @see <a href="https://github.com/semver/semver/issues/232">semver/semver#232</a>
	 */
	private static final Pattern SEMVER_PATTERN = Pattern.compile("^" + "(?<Major>0|[1-9]\\d*)\\." + "(?<Minor>0|[1-9]\\d*)\\." + "(?<Patch>0|[1-9]\\d*)" + "(-(?<PreReleaseTag>" + PRE_RELEASE_PATTERN_STRING + "))?" + "(\\+(?<BuildMetadataTag>" + BUILD_METADATA_PATTERN_STRING + ")" + ")?$");

	int major;
	int minor;
	int patch;
	String preRelease;
	String buildMetadata;

	private Version(int major, int minor, int patch, String preRelease, String buildMetadata) {
		this.major = major;
		this.minor = minor;
		this.patch = patch;
		this.preRelease = preRelease;
		this.buildMetadata = buildMetadata;
	}

	/**
	 * Creates a Version from a semver string.
	 *
	 * @throws IllegalArgumentException if the given String doesn't match the semver regex from semver.org.
	 */
	public static Version of(String fullVersionString) {
		Matcher matcher = SEMVER_PATTERN.matcher(fullVersionString == null ? "null" : fullVersionString);
		if (!matcher.matches()) {
			throw new IllegalArgumentException(fullVersionString + " is not a semver String.");
		}

		int major = Integer.parseInt(matcher.group("Major"));
		int minor = Integer.parseInt(matcher.group("Minor"));
		int patch = Integer.parseInt(matcher.group("Patch"));
		String preRelease = matcher.group("PreReleaseTag");
		String buildMetadata = matcher.group("BuildMetadataTag");

		return of(major, minor, patch, preRelease, buildMetadata);
	}

	/**
	 * Creates a Version from its individual parts.
	 *
	 * @param preRelease    the prerelease part of the semver. Can be null.
	 * @param buildMetadata the build metadata of the version. Can be null.
	 * @throws IllegalArgumentException if the preRelease or buildMetadata is not null and doesn't match the parts of
	 *                                  the regex semver regex.
	 */
	public static Version of(int major, int minor, int patch, String preRelease, String buildMetadata) {
		validatePreRelease(preRelease);
		validateBuildMetadata(buildMetadata);

		return new Version(major, minor, patch, preRelease, buildMetadata);
	}

	public static Version of(int major, int minor, int patch, String preRelease) {
		return of(major, minor, patch, preRelease, null);
	}

	public static Version of(int major, int minor, int patch) {
		return of(major, minor, patch, null, null);
	}

	private static void validatePreRelease(String preRelease) {
		if (preRelease != null && !PRE_RELEASE_PATTERN.matcher(preRelease).matches()) {
			throw new IllegalArgumentException(preRelease + " is not a valid preRelease");
		}
	}

	private static void validateBuildMetadata(String buildMetadata) {
		if (buildMetadata != null && !BUILD_METADATA_PATTERN.matcher(buildMetadata).matches()) {
			throw new IllegalArgumentException(buildMetadata + " is not a valid buildMetadata");
		}
	}

	/**
	 * @return the Comparator that is equivalent to {@link #compareTo(Version)}
	 * @implNote When two Version objects differ only in buildMetadata, then this comparator compares them as equal
	 * while the equals doesn't
	 */
	public static Comparator<Version> getComparator() {
		return COMPARATOR;
	}

	/**
	 * Compares this version with the other version.
	 *
	 * @param other the Version to compare.
	 * @throws NullPointerException if the other version is {@code null}.
	 * @implNote Versions that are equal in all except the build metadata will compare with 0, but not be equal.
	 */
	@Override
	public int compareTo(@NonNull Version other) {
		return getComparator().compare(this, other);
	}

	@Override
	public String toString() {
		return major + "." + minor + "." + patch + (preRelease != null && !preRelease.isEmpty() ? "-" + preRelease : "") + (buildMetadata != null && !buildMetadata.isEmpty() ? "+" + buildMetadata : "");
	}

	@Override
	public int hashCode() {
		return Objects.hash(major, minor, patch, preRelease, buildMetadata);
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}

		if (!(other instanceof Version version)) {
			return false;
		}

		return version.major == this.major && version.minor == this.minor && version.patch == this.patch && Objects.equals(version.preRelease, this.preRelease) && Objects.equals(version.buildMetadata, this.buildMetadata);
	}
}
