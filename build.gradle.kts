plugins {
	java
	id("io.freefair.lombok") version "8.6"
	id("org.sonarqube") version "5.0.0.4638"
	jacoco
}

group = "de.jball.java"
version = "0.1.0-SNAPSHOT"

repositories {
	mavenCentral()
}

dependencies {
	testImplementation(platform("org.junit:junit-bom:5.10.2"))
	testImplementation("org.junit.jupiter:junit-jupiter")
}

java {
	sourceCompatibility = JavaVersion.VERSION_17
	targetCompatibility = JavaVersion.VERSION_17
}

sonar {
	properties {
		property("sonar.projectKey", "fibsifan_semver4j")
		property("sonar.organization", "fibsifan")
		property("sonar.coverage.jacoco.xmlReportPaths", "build/reports/jacoco/test/jacocoTestReport.xml")
	}
}

tasks {
	test {
		useJUnitPlatform()
		finalizedBy("jacocoTestReport")
	}

	wrapper {
		gradleVersion = "8.9"
		distributionType = Wrapper.DistributionType.ALL
	}

	jacocoTestReport {
		reports {
			xml.required = true
		}
	}
}
